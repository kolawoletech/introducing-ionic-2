# Awesome iTunes Browser

This is the companion code for the course "Introducing Ionic2" by Mathieu Chauvinc, published by [Packt](http://packtpub.com)

## Latest branch

Please make sure to use the branch **alpha56** to get the code corresponding to the latest update of the course and Ionic2.

## Change log

1. Section 3 video 3: Markup has changed for ion-radio. Refer to tag after_s3v3
1. Section 6 video 3: Markup has changed for ion-input. Refer to tag after_s6v3
1. Section 7 video 4: Selecting country requires to adopt the new ion-radio (as per s3v3). The change is made at section 7 video 1 level. Use commit 11c4ff82fc06834b8f0463b89adeb439d785e9cd before s7v4

## How to use

### Getting the code for a specific video

Simply use git checkout with the [appropriate tag](#markdown-header-how-to-use).
```sh
$ git checkout s3v1
```
Note that you will then be in a detached HEAD state so you might want to
```sh
$ git checkout -b a_new_branch
```
in order to keep your own code changes.

### How do tags work?

Each section and each video have 2 tags corresponding to their start and end.

Sections follow the pattern "s<section number>_start" or "s<section number>_start". For example `s3_start` is the code as it is at the beginning of section 3; and the code at the end of section 5 would be found with the tag `s5_end`.

Videos follow the pattern "s<section number>v<video number>" for start and "after_s<section number>v<video number>" for end. For example `s4v2` is the code at the beginning of the 2nd video of section 4. And the code after the first video of section would be at tag `after_s2v1`
